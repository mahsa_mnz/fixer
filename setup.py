from setuptools import setup, find_packages

VERSION = '0.0.2'
DESCRIPTION = 'My first Python package'
LONG_DESCRIPTION = 'My first Python package with a slightly longer description'

# Setting up
setup(
    # the name must match the folder name 'verysimplemodule'
    name="fixersimplemodule",
    version=VERSION,
    author="Mahsa Monazami",
    author_email="<mahsa,mnz@gmail.com>",
    description=DESCRIPTION,
    long_description=LONG_DESCRIPTION,
    packages=find_packages(),
    install_requires=[
        'requests'
    ],
    keywords=['python', 'fixer'],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Intended Audience :: Education",
        "Programming Language :: Python :: 3",
        "Operating System :: MacOS :: MacOS X",
        "Operating System :: Microsoft :: Windows",
    ],
    url="https://gitlab.com/mahsa_mnz/fixer.git"
)
