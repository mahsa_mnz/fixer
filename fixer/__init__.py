from fixer.get_rates_handler import call_fixer_api as get_rates

__all__ = [
    'get_rates'
]
