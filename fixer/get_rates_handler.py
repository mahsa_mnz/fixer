import requests
from json import loads

FIXER_BASE_URL = 'http://data.fixer.io/api/latest?access_key='


def call_fixer_api(api_key):
    response = requests.get(FIXER_BASE_URL + api_key)
    if response.status_code == 200:
        content = loads(response.text)
        if content['success']:
            return content
    return None
